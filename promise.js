let readline = require('readline-sync');

//TODO membuat fungsi untuk implementasi Promise
function isMe(name) {
  return new Promise((resolve, reject) => {
    if (name !== 'Dyaz Amrullah') {
      resolve(`${name} bukan nama saya`)
    } else {
      reject(`${name} adalah nama saya`)
    }
  })
}

let name = readline.question("Masukan nama saya: ");

//* Promise
// isMe(name)
//   .then(data => console.log(data))
//   .catch(err => console.log(err));

//* Async Await
async function main() {
  try {
    let hasil = await isMe(name);
    console.log(hasil);
  } catch (error) {
    console.log(error);
  }

}
main();

