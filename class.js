
//TODO Membuat Class untuk implementasi instance property & method
class Person {
  //* instance property
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    // this.sayHello = function (name) {
    //   console.info(`Hello ${name}, my name is ${this.firstName} ${this.lastName}`);
    // };
  }

  //* instance methode 
  sayHello = function (name) {
    console.info(`Hello ${name}, my name is ${this.firstName} ${this.lastName}\n`);
  };
}

//add new static method
Person.work = (name) => {
  return `${name} Sedang bekerja`;
}

//add new instance method
Person.prototype.greeting = (name) => {
  return `hai ${name}, semoga hari-harimu lebih baik ya`;
}

//panggil static method
console.log(Person.work('Sandhika Galih'));

//panggil instance method
const dyaz = new Person("Dyaz", "Amrullah");
dyaz.sayHello("Monica");
console.log(dyaz.greeting('Umar'));



//TODO Membuat Class untuk implementasi static property & method
class MathUtil {
  //* static property
  static title = "Penjumlahan Dari Nilai-Nilai";

  //* static method
  static sum(...numbers) {
    let total = 0;
    for (const number of numbers) {
      total += number;
    }
    return total;
  }

}

//panggil static method
console.log(MathUtil.title);
const result = MathUtil.sum(10, 10, 10, 10, 10);
console.log(`Hasil Penjumlahan : ${result}`);

